import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export const store = new Vuex.Store({
    strict: true,
    state: {
        products: [
            {id: 1, name: 'pran-sos', price: 202, active: true},
            {id: 2, name: 'potato', price: 10, active: true},
            {id: 3, name: 'pran-up', price: 30, active: false},
            {id: 4, name: 'candy', price: 2, active: true},
        ]
    },
    getters: {
        activeProducts(state)
        {
            var active = state.products.map(product => {
                return {
                    id: product.id,
                    name: '**' + product.name + '**',
                    price: product.price/2,
                    active: product.active,
                }
            })
            return active;
        },
        activePro(state){
            return state.products.filter(product => {
                return product.active;
            })
        }

    },
    mutations: {
        reducePrice(state, payload)
        {
            state.products.forEach(product => {
                product.price -= payload;
            })
        },
        addNewProduct(state, newProducts)
        {
            state.products.push(newProducts)
        },
        deleteProduct(state, index)
        {
            state.products.splice(index, 1)
        }
    },
    actions: {
        reducePrice: (context, payload) => {
            setTimeout(function () {
                context.commit('reducePrice', payload);
            },2000)
        },
        addNewProduct(context, newProducts)
        {
            context.commit('addNewProduct', newProducts)
        },
        deleteProduct(context, index)
        {
            context.commit('deleteProduct', index)
        }
    }
})
